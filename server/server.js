var http = require('http');
var express = require('express');
var app = express();
var bodyParser = require('body-parser');
var server = http.createServer(app);

var dbPath = __dirname + '/db/';
var Datastore = require('nedb');
var db = {};
db.users = new Datastore({ filename: dbPath + 'users', autoload: true });
db.config = new Datastore({ filename: dbPath + 'config', autoload: true });
var io = require('socket.io').listen(server); 
var Server = {
  init: function() {
    server.listen(4000)
    app.use(express.static(__dirname));
    app.use(bodyParser.urlencoded({ extended: false })); 
    app.all('/', function(req, res, next) {
      res.header("Access-Control-Allow-Origin", "*");
      res.header("Access-Control-Allow-Headers", "X-Requested-With");
      next();
    });    
    app.get('/', function(req, res){
       res.sendfile(__dirname + '/index.html');
    });
    app.get('/camera', function(req, res){
       res.sendfile(__dirname + '/camera.html');
    });
    app.post('/login', function(req, res) {
        db.users.find(req.body, function(err, user) {
          if (err || user.length <= 0) res.status(500).jsonp({ message: 'Invalid username or pasword' });
          else {
            res.send(user);
          }
        });      
    });
    
    io.on('connection', function (socket) {
      socket.on('capture', function (data) {
        socket.broadcast.emit('trigger-capture', data);                
      });
      socket.on('alarm', function (data) {
        socket.broadcast.emit('trigger-alarm', {});                
      });
    });
  },
  db: function() {
    return db;
  }
};

module.exports = Server;
