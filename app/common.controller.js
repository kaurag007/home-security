angular.module('monitoring')
.controller('CommonCtrl', ['$rootScope', '$scope', 'Popeye', 'datastore', 'util', function($rootScope, $scope, Popeye, datastore, util) {
	$scope.user = datastore.user;
	$scope.selectedMenu = 'camera';
	$rootScope.config = {
		motion: {},
		phoneNumber: ''
	};
	
	$scope.getConfig = function() {
		datastore.getConfig().then(function(config) {			
			$scope.$apply($rootScope.config = config);						
		}, function() {
			console.log('No config detected');
		});
	}();
	$scope.$watch(function() {
		return datastore.user;
	}, function(newVal) {
		$scope.user = newVal;
		console.log(newVal);
	});
	$scope.about = function() {
		var modal = Popeye.openModal({
				templateUrl: "app/about.html",
				controller: "AboutCtrl"								
			});
	};
	
	$scope.logout = function() {
		datastore.user = undefined;
		location.reload();
	};
		
	$scope.config = function() {
		var modal = Popeye.openModal({
				templateUrl: "app/config.html",
				controller: "ConfigCtrl"								
			});
	};
	
	$scope.users = function() {
		var modal = Popeye.openModal({
				templateUrl: "app/user/users.html",
				controller: "UserCtrl"								
			});
	};
	
	$scope.remote = function() {
		var modal = Popeye.openModal({
				templateUrl: "app/monitoring/enter_ip.html",
				controller: 'RemoteCtrl'
			});		
	};
	  
}])
.controller('AboutCtrl', ['$rootScope', '$scope', 'Popeye', function($rootScope, $scope, Popeye) {	
	$scope.isOnline = false;
	$scope.version = 'v1.0.0';
	require('dns').lookup(require('os').hostname(), function (err, add, fam) {
		$scope.$apply($rootScope.ipAddress = add);
	});
}])
.controller('ConfigCtrl', ['$rootScope', '$scope', 'datastore', 'Popeye', function($rootScope, $scope, datastore, Popeye) {	
	$scope.videoPath = __dirname + '/records/videos/';
	$scope.imagePath = __dirname + '/records/images/';
	
	$scope.selectDirectory = function(path) {		
		var shell = require('shell');		
		shell.openItem(path);
	};
	
	$scope.saveConfig = function() {
		datastore.updateConfig($rootScope.config).then(function() {
			swal("Saved!", "Configuration has been updated.", "success");		
		}, function(err) {
			sweetAlert("Oops...", err , "error");
		});
	};
}]);