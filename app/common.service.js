angular.module('monitoring')
.service('datastore', function() {
	this.user;
	var dbPath = __dirname + '/server/db/';
	var Datastore = require('nedb');
	var db = require('./server/server.js').db();
	
	db.users.ensureIndex({ fieldName: 'username', unique: true }, function (err) {
		if (err) console.log(err);
	});
	this.getUsers = function() {
		return new Promise(function(resolve, reject) {
			db.users.find({}, function(err, docs) {
				if (err || _.isEmpty(docs)) reject(err);
				else resolve(docs);
			});
		})
	};
	
	this.addUser = function(user) {
		return new Promise(function(resolve, reject) {
			db.users.insert(user, function(err, newUser) {
				if (err) reject(err);
				else resolve(newUser);
			});			
		});
	};
	
	this.login = function(user) {
		var self = this;
		return new Promise(function(resolve, reject) {
			db.users.findOne(user, function(err, user) {
				if (err || _.isEmpty(user)) reject(err);
				else {
					if (user.role !== 'Viewer') {
						self.user = user;
					}				
					resolve(user);
				}
			});
		})
	};
	
	this.updateUser = function(user) {
		return new Promise(function(resolve, reject) {
			db.users.update( {_is: user.id }, user, {}, function(err, updatedUser) {
				if (err) reject(err);
				else resolve(updatedUser);
			});
		});
	};
	
	this.removeUser = function(userId) {
		return new Promise(function(resolve, reject) {
			db.users.remove( { _id: userId }, {}, function(err, numRemoved) {
				if (err) reject(err);
				else resolve(numRemoved);
			});
		});
	};
	
	this.getConfig = function() {
		return new Promise(function(resolve, reject) {
			db.config.findOne( {}, function(err, configList) {
				if (err || _.isEmpty(configList)) reject(err);
				else resolve(configList);
			});
		});
	};
	
	this.updateConfig = function(data) {
		return new Promise(function(resolve, reject) {
			db.config.update( { _id: data._id }, data, { upsert: true }, function(err, doc) {
				if (err) reject(err);
				else resolve(doc);
			});
		});
	}
})
.service('util', function($http) {
	var request = require('request');	
	this.sendSMS = function(cameraNumber, phoneNumber) {
		var chikkaUrl = 'https://post.chikka.com/smsapi/request';
		var data = {
			'message_type' : 'SEND',
			'mobile_number': phoneNumber,
			'message_id': (Math.floor(Math.random() * 10000)) + '00',
			'message': 'Motion detected on camera ' + cameraNumber + ' @' + moment().format('MMM. DD, YY - HH:mm:ss'),
			'client_id': '332b78a48ef92c4f84940feaa07bf79effbb8925a8cf79f055a02d6e647e0a10',
			'secret_key': '6faa88f4c9bf00cfe6ee6777e5f031a46d6f092fd231f08454d329162621ccc4',
			'shortcode': '29290131117' 
		};
	
		request.post({url: chikkaUrl, form:  data}, function(err,httpResponse,body){ 
			console.log(err);
		});	
	}
});