/* App module */
angular.module('monitoring', [
    'ui.router',
    'pathgather.popeye'
]).config(['$urlRouterProvider', '$stateProvider', '$httpProvider',
    function($urlRouterProvider, $stateProvider, $httpProvider) {
        $stateProvider
            .state('home', {
                url: '/home',
                templateUrl: 'app/home.html',                
            })
            .state('monitoring', {
                url: '/monitoring',
                templateUrl: 'app/monitoring/monitoring.html',
                controller: 'MonitoringCtrl'
            })
            .state('users', {
                url: '/user',
                templateUrl: 'app/user/users.html',
                controller: 'UserCtrl'
            })
            .state('login', {
                url: '/user/login',
                templateUrl: 'app/user/login.html',
                controller: 'LoginCtrl'
            })

        $urlRouterProvider.otherwise('/not_found');
        $httpProvider.defaults.headers.common['Accept'] = 'application/json, text/javascript';
        $httpProvider.defaults.headers.common['Content-Type'] = 'application/json; charset=utf-8';
        
    }
]).run(['$rootScope', '$location',  function($rootScope, $location) {    
    $location.path('/user/login');
}]);