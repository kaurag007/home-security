angular.module('monitoring')
.directive('camera', function(util) {
	return {
		restrict: 'A',
		templateUrl: 'app/monitoring/camera.html',
		link: function(scope, elem, attr) {
            var fs = require('fs');
            var chunkInterval;
            scope.recording = false;
            scope.cameraReady = false;
            scope.connection.selectDevices(scope.camera.id);
            scope.connection.dontCaptureUserMedia = false;
            scope.connection.sdpConstraints.mandatory = {
                OfferToReceiveAudio: false,
                OfferToReceiveVideo: false
            };
            
            /*
            / Video recording
            */
            
            function startRecord(stream) {           
                if (!scope.recording) {
                    scope.$apply(scope.recording = true);                              
                    scope.recordVideo = RecordRTC(stream, {
                        type: 'video'
                    });                        
                    scope.recordVideo.startRecording();
                    
                    chunkInterval = setInterval(function() {
                        scope.recordVideo.stopRecording(function() {
                            chunkRecord();                            
                        });                                        
                    }, 60000 * 2);   
                }
            };
            
            function chunkRecord(stop) {
                scope.recordVideo.getDataURL(function(videoDataURL) {
                    if (!stop) {
                        scope.recordVideo.initRecorder();                                                    
                    }
                    saveChunk(videoDataURL);
                });
            };
            
            function stopRecording() {
                if (scope.recording) {
                    scope.$apply(scope.recording = false);
                    scope.recordVideo.stopRecording(function() {
                        chunkRecord(true);                            
                    });
                    clearInterval(chunkInterval);                                                
                }
            };
            
            function saveChunk(videoUrl, callback) {
                var camNumber = scope.$index + 1;
                var videoFile = {
                    name: moment().format('MMM_DD_YY_HH_mm_ss_') +  'camera' + camNumber + '.mp4',
                    type: 'video/mp4',
                    contents: videoUrl
                };                
                var filePath = __dirname + '/records/videos/camera' + camNumber + '/' + videoFile.name;
                var content = videoFile.contents.split(',').pop();                                                                      
                var fileBuffer = new Buffer(content, "base64");
            
                fs.writeFileSync(filePath, fileBuffer);
            };
            
            /*
            / Motion detection
            */
            var sensitivity, temp1Canvas, temp1Context, temp2Canvas, temp2Context;
            var rendering = false;        
            var currentImage = null;
            var oldImage = null;
            var matched = true;
            var detected;
            var width = 64;
            var height = 48;
            var video = angular.element(elem).find('video')[0];
            var detectInterval;
                        
            function initDetect() {
                initCanvas();
                detectInterval = setInterval(main.bind(this), 500);
            };
            
            function main() {
                if (scope.motion) {
                    try{
                        render();
                    } catch(e) {
                        console.log(e);
                        return;
                    }                    
                }

            };
                                                            
            function render() {
                oldImage = currentImage;
                currentImage = captureImage();
    
                if(!oldImage || !currentImage) {
                    return;
                }
    
                compare(currentImage, oldImage, width, height);
    
            }
            
            function initCanvas() {
                sensitivity = 40;
    
                if(!temp1Canvas) {
                    temp1Canvas = document.createElement('canvas');
                    temp1Context = temp1Canvas.getContext("2d");
                }
    
                if(!temp2Canvas) {
                    temp2Canvas = document.createElement('canvas');
                    temp2Context = temp2Canvas.getContext("2d");
                }
            };
            
            function compare(image1, image2, width, height) {
                initCanvas();
    
                if(!image1 || !image2) {
                    return;
                }
    
                temp1Context.clearRect(0,0,100000,100000);
                temp1Context.clearRect(0,0,100000,100000);
    
                temp1Context.drawImage(image1, 0, 0, width, height);
                temp2Context.drawImage(image2, 0, 0, width, height);
                    
                if (matched) {
                    for(var y = 0; y < height; y++) {
                        for(var x = 0; x <  width; x++) {
                            var pixel1 = temp1Context.getImageData(x,y,1,1);
                            var pixel1Data = pixel1.data;
        
                            var pixel2 = temp2Context.getImageData(x,y,1,1);
                            var pixel2Data = pixel2.data;
        
                            worker.compare(pixel1Data, pixel2Data, function(matches) {
                                matched = matches;
                            });
                        }
                    } 
                } else {
                    motionDetected();
                }
            };
                        
            function captureImage() {                
                var canvas = document.createElement('canvas');                                
                canvas.width = width;
                canvas.height = height;
                canvas.getContext('2d').drawImage(video, 0, 0, width, height);
                         
                return canvas;
            };
            
            function motionDetected() {
                if (scope.motion) {
                    if (!matched && !detected) {
                        var config = scope.config.motion;
                        if (config.capture_image) {
                            scope.capture(scope.camera.deviceId, scope.$index).then(function(content) {
                                scope.socket.emit('capture', { content: content });
                            });                            
                        }
                        
                        if (config.tragger_alarm) {
                            var audio = new Audio('assets/alarm.mp3');
                            audio.play();
                             scope.socket.emit('alarm', {});                            
                        }
                        
                        if (config.send_sms) {
                            util.sendSMS(scope.$index + 1, scope.config.phoneNumber);
                        }
                        detected = setTimeout(function() {
                            matched = true;
                            detected = undefined;
                        }, 10000);
                    
                    }                    
                }
            };
            
            var worker = operative({
                getPixel: function(binaryData1, binaryData2, width, height, cb) {
                    var move = false;
                    for(var y = 0; y < height; y++) {
                        for(var x = 0; x <  width; x++) {
                            var pixel1 = binaryData1[y + x];                            
                            var pixel2 = binaryData2[y + x];
                            
                            this.compare(pixel1, pixel2, function(matches) {
                                if (!matches) {
                                    move = true;
                                }
                            });		
                        }
                    }
                    cb(move);
                },
                compare: function(p1, p2, cb) {
                    var sensitivity = 60;
                    var matches = true;

                    for(var i = 0; i < p1.length; i++) {
                        var t1 = Math.round(p1[i]/10)*10;
                        var t2 = Math.round(p2[i]/10)*10;
        
                        if(t1 != t2) {
                            if((t1+sensitivity < t2 || t1-sensitivity > t2)) {
                                matches = false;
                            }
                        }
                    }
                    cb(matches);
                }
            });
            
            /*
            / Video broadcasting
            */
            
            var connectedRoom;
            
            var config = {
                openSocket: function(config) {
                    var channel = 'homeko'
                    var socket = new Firebase('https://webrtc.firebaseIO.com/' + channel);
                    socket.channel = channel;
                    socket.on("child_added", function(data) {
                        config.onmessage && config.onmessage(data.val());
                    });
                    socket.send = function(data) {
                        this.push(data);
                    };
                    config.onopen && setTimeout(config.onopen, 1);
                    socket.onDisconnect().remove();
                    return socket;
                },
                onRemoteStream: function(htmlElement) {
                },
                onRoomFound: function(room) { 
                }                  
            };
            var broadcastUI = broadcast(config);

            scope.connection.captureUserMedia(function(stream) {
                    scope.connection.dontCaptureUserMedia = true;
                    var deviceId = scope.camera.deviceId;
                    var canvas = document.getElementById("canvas-" + scope.camera.deviceId);
                    var context = canvas.getContext("2d");
                    var cameraVideo = angular.element(elem).find('video')[0];
                    var fullScreenBtn = document.getElementById('full-screen-' + deviceId);
                    var startRecordBtn = document.getElementById('record-' + deviceId);
                    var stopRecordBtn = document.getElementById('stop-record-' + deviceId);
                    var motionDetectionBtn = document.getElementById('motion-' + deviceId);
                    var broadcastBtn = document.getElementById('broadcast-' + deviceId);
                    var streamFile = window.URL.createObjectURL(stream);
                    canvas.width = cameraVideo.videoWidth;
                    canvas.height = cameraVideo.videoHeight;
                    scope.camera.stream = stream;
                    config.attachStream = stream;                        
                                                
                    cameraVideo.src = streamFile;                            
                    cameraVideo.play();

                    startRecordBtn.addEventListener('click', function() {
                        startRecord(stream);                                
                    });
                    
                    stopRecordBtn.addEventListener('click', function() {
                        stopRecording(); 
                    });
                    
                    broadcastBtn.addEventListener('click', function() {  
                        scope.$apply(scope.broadcast = !scope.broadcast);                      
                        broadcastUI.createRoom({
                            roomName: 'Anonymous',
                            isAudio: false
                        });                 
                    });
                    
                    motionDetectionBtn.addEventListener('click', function() {
                        scope.$apply(scope.motion = !scope.motion);
                        if (scope.motion) {
                            setTimeout(function() {
                                initDetect();                                
                            }, 60000 * 2);                            
                        } else {
                            matched = false;
                        }
                    });
                                                                                
                    fullScreenBtn.addEventListener('click', function() {
                        if (cameraVideo.requestFullscreen) {
                        cameraVideo.requestFullscreen();
                        } else if (cameraVideo.mozRequestFullScreen) {
                        cameraVideo.mozRequestFullScreen();
                        } else if (cameraVideo.webkitRequestFullscreen) {
                        cameraVideo.webkitRequestFullscreen();                           
                        } 
                    })                                 
            
                });    
		}
	}
})