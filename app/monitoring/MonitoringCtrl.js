angular.module('monitoring')
.controller('MonitoringCtrl', ['$rootScope', '$scope', '$state', function($rootScope, $scope, $state) {
	
	$rootScope.socket = io('http://localhost:4000');
		
	$scope.init = function() {
		if (!$rootScope.connection) {
			$rootScope.cameraList = [];
			$scope.time = Date();
			$rootScope.connection = new RTCMultiConnection();
			$rootScope.connection.body = document.getElementById('videos-container');
			$rootScope.connection.direction = 'one-way';
			$rootScope.connection.DetectRTC.load(function() {
				$rootScope.connection.enumerateDevices(function(devices) {
					// iterate over devices-array
					// if (devices.length >= 3) {
					// 	devices.splice(0, 1)
					// }
					devices.forEach(function(device) {
						if (device.kind.indexOf('audio') != -1 || device.label.search('Built-in') != -1) return;					
						$scope.$apply($rootScope.cameraList.push(device));
						console.log(device);
					});
				})
			});			
		}
	};
	
	$scope.capture = function(deviceId, index) {
		return new Promise(function(resolve, reject) {
			var scale = 0.25;
			var video = document.getElementById("video-" + deviceId);
			var canvas = document.getElementById("canvas-" + deviceId);
			var context = canvas.getContext("2d");
			canvas.width = video.videoWidth;
			canvas.height = video.videoHeight;
			context.drawImage(video, 0, 0, canvas.width, canvas.height);
			
			setTimeout(function() {
				var fs = require('fs');
				$scope.$apply($scope.closeSnapshot = true);			
				var content = canvas.toDataURL('image/jpeg');
				var fileBuffer = new Buffer(content.split(',').pop(), "base64");
				var fileName = moment().format('MMM_DD_YY_HH_mm_ss') + '_cam' + ( index + 1 ) + '_snapshot.jpeg';            
				fs.writeFileSync(__dirname + '/records/images/' + fileName, fileBuffer);
				
				resolve(content);
				setTimeout(function() {
					$scope.$apply($scope.closeSnapshot = false);
					context.clearRect(0, 0, 1200, 1000);
				}, 1000); 
			}, 1000);
		});		
	}
}])
.controller('RemoteCtrl', ['$scope', function($scope) {
	$scope.submit = function() {
		sweetAlert("Oops...", "Cannot connect to remote camera!", "error");
	}
}]);