angular.module('monitoring')
.controller('LoginCtrl', ['$scope', '$state', 'datastore', function($scope, $state, datastore) {
	$scope.username = '';
	$scope.password = '';
	$scope.submit = function() {
		datastore.login({username: $scope.username, password: $scope.password})
		.then(
			function(user) {
			if (user.role === 'Admin') {
				$state.go('monitoring')				
			} else {
				sweetAlert("Oops...", "Your account is for view only!", "error");	
			}
		}, 	function() {
			sweetAlert("Oops...", "Invalid username or password!", "error");			
		});
	};	
}]);