angular.module('monitoring')
.controller('UserCtrl',
	['$scope', 'datastore', 'Popeye', function($scope, datastore, Popeye) {
		$scope.users = [];
		$scope.getUsers = function() {
			datastore.getUsers().then(function(users) {
				$scope.$apply($scope.users = users);
			});
		}
		
		$scope.addUser = function() {
			var modal = Popeye.openModal({
				templateUrl: "app/user/new_user.html",
				controller: "NewUserCtrl",
				resolve: {
					type: function() {
						return 'add'; 
					}
				}				
			});
			
			modal.closed.then(function() {
				$scope.getUsers();
			});
		}
		
		$scope.removeUser = function(userId) {
			swal({   
				title: "Are you sure?",   
				text: "You want to remove this user",
				type: "warning",
				showCancelButton: true,
				confirmButtonColor: "#d9534f",
				confirmButtonText: "Yes, delete it!",
				closeOnConfirm: false 
				}, function(){
					datastore.removeUser(userId).then(function() {
						swal("Deleted!", "User has been deleted.", "success");
						$scope.getUsers();						
					}, function() {
						sweetAlert("Oops...", "Error while removing user!", "error");
					});
				});
		}
	}]
).controller('NewUserCtrl', function($scope, datastore, Popeye, type) {
	$scope.user = {};
	
	$scope.submit = function() {
		if ($scope.confirmPassword !== $scope.user.password) {
			alert('Password not matched');
		} else {
			$scope.user.dateCreated = new Date();
			datastore.addUser($scope.user).then(function(user) {
				Popeye.closeCurrentModal();
			}, function(err) {
				sweetAlert("Oops...", err , "error");
			});
		}
	}
});