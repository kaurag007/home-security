angular.module('monitoring')
.directive('menu', function() {
	return {
		restrict: 'E',
		templateUrl: 'app/home.html',
		link: function(scope, elem, attr) {			
		}
	}
})
.directive('time', function() {
	return {
		restrict: 'A',
		link: function(scope, elem, attr) {
			angular.element(elem).html(moment().format('MMM. DD, YY - HH:mm:ss'));
			setInterval(function() {
				angular.element(elem).html(moment().format('MMM. DD, YY - HH:mm:ss'));
			}, 1000);
		}		
	}
});